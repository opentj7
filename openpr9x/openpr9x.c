/* Copyright 2009 Anonymous
 *
 *
 * This file is part of OpenPR9X.
 *
 * OpenPR9X is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * OpenPR9X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenPR9X. If not, see <http://www.gnu.org/licenses/>.
 */

#include <memory.h>
#include "openpr9x.h"

/* One LCG round: x_{n+1} = (a*x_n + b) % mod;
 * a = 9; b = 7;
 */
int Gpsch_u(int x, int mod)
{
	const int a = 9, b = 7;

	/* Never fail to fail */
	if (!mod)
		mod = 1;

	return (a*x + b) % mod;
}

/* Constant S-box used in keystream transform */
enum {Sbox_size = 256};
static char Sbox[Sbox_size];

/* Initialize S-box to some byte permutation */
int PR9X_init()
{
	int i;

	const int mod0 = 256;

	for (i = 0; i < Sbox_size; i++) {
		Sbox[i] = (char)Gpsch_u(i, mod0);
	}
	
	return 0;
}

/* PR9X core transform:
 * Stream cipher (add/sub) with simple periodic keystream generation.
 * Strenghtened by padding with 8 bit derived key.
 */
static int PR9X_xfrm(sstr_p outs, const sstr_p ins, const sstr_p key,
                     const int decrypt)
{
	struct short_string keystream;
	char* keystr_p;

	unsigned int ti, ki;
	int seed, rngout;

	const int mod1 = 256, mod2 = 255;

	if (!ins->size)
		return -1;

	outs->size = ins->size;
	memcpy(&outs->str, &ins->str, ins->size);

	if (!key->size)
		return 0;

	keystream.size = key->size;
	memcpy(&keystream.str, &key->str, key->size);
	keystr_p =  keystream.str;

	/* Keystream is generated from periodically repeated raw key,
	   transformed via constant S-box at each repetition */

	for (ti = 0, ki = 0; ti < ins->size; ti++, ki++) {
		if (ki >= key->size)
			ki = 0;

		if (decrypt)
			outs->str[ti] -= keystr_p[ki];
		else
			outs->str[ti] += keystr_p[ki];

		keystr_p[ki] = Sbox[(unsigned char)keystr_p[ki]];
	}

	/* 8 bit pad key is derived from transformed key */

	seed = (key->size + 1)*key->size/2;

	for (ki = 0; ki < key->size; ki++)
		seed += keystr_p[ki];
	
	rngout = Gpsch_u(seed, mod1) + Gpsch_u(seed, mod2);

	for (ti = 0; ti < ins->size; ti++)
		if (decrypt)
			outs->str[ti] -= (char)rngout;
		else
			outs->str[ti] += (char)rngout;

	return 0;
}

int Shifr_u(sstr_p ciphertext, const sstr_p plaintext, const sstr_p key)
{
	return PR9X_xfrm(ciphertext, plaintext, key, 0);
}

int NShifr_u(sstr_p ciphertext, const sstr_p plaintext, const sstr_p key)
{
	return PR9X_xfrm(ciphertext, plaintext, key, 1);
}
