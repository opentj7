/* Copyright 2009 Anonymous
 *
 *
 * This file is part of OpenPR9X.
 *
 * OpenPR9X is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * OpenPR9X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenPR9X. If not, see <http://www.gnu.org/licenses/>.
 */

struct short_string
{
	unsigned char size;
	char str[255];
};

typedef struct short_string* sstr_p;

/* Initialize internal tables */
int PR9X_init();

/* One LCG round: x_{n+1} = (a*x_n + b) % mod;
 * a = 9; b = 7;
 */
int Gpsch_u(int x, int mod);

/* PR9X basic encryption, text size < 255 */
int Shifr_u(sstr_p ciphertext, const sstr_p plaintext, const sstr_p key);

/* PR9X basic decryption, text size < 255 */
int NShifr_u(sstr_p plaintext, const sstr_p ciphertext, const sstr_p key);

